package composite;



public class Main {
    public static void main(String[] args) {

        MenuComponent breakfastMenu =
                new Menu("MENU ŚNIADANIOWE", "MS");

        MenuComponent dinnerMenu =
                new Menu("MENU LUNCH", "LUNCH");

        MenuComponent alcoholMenu =
                new Menu("MENU %", "%");

        MenuComponent allMenus =
                new Menu("MENU", "All menu");

        allMenus.add(breakfastMenu);
        allMenus.add(dinnerMenu);
        allMenus.add(alcoholMenu);

        MenuComponent beer =
                new MenuItem(4.0, "Harnas",
                        "Pyszne piwko", false);

        MenuComponent eggs =
                new MenuItem(4.0, "Jajka",
                        "Jajka na twardo",
                        true);

        alcoholMenu.add(beer);
        breakfastMenu.add(eggs);

        Waitress waitress = new Waitress(allMenus);

        waitress.printAlcohol();

    }

}
