package composite;


public class Waitress {

    private MenuComponent allMenus;

    public Waitress(MenuComponent allMenus) {
        this.allMenus = allMenus;
    }

    public void print() {
        System.out.println(allMenus.print());
    }

    public void printBreakfast() {
        findMenu("Breakfast");
    }

    public void printAlcohol() {
        findMenu("MENU %");
    }

    private void findMenu(String menuName) {
        allMenus.createComponentStream()
                .filter(e -> e.getName().equalsIgnoreCase(menuName))
                .findFirst()
                .ifPresentOrElse(
                        e -> System.out.println(e.print()),
                        () -> System.out.println("No Menu found")
                );
    }

}
