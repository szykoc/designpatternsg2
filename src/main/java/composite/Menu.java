package composite;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

// klasa Node, służy do przechowania całego Menu
// np Menu sniadaniowe które będzie miało kolejne MenuItemy..

public class Menu extends MenuComponent {
    private String name;
    private String description;
    List<MenuComponent> menuComponents = new ArrayList<>();

    public Menu(String name, String description){
        this.name = name;
        this.description = description;
    }

    @Override
    public void add(MenuComponent component) {
        menuComponents.add(component);
    }

    @Override
    public void remove(MenuComponent component) {
        menuComponents.remove(component);
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public Stream<MenuComponent> createComponentStream() {
        return menuComponents.stream();
    }

    @Override
    public String print() {
        return
                "\n" + getName() + " " +
                getDescription() + "\n" +
                "-----------------------------------------" +
                menuComponents.stream()
                              .map(MenuComponent::print)
                              .collect(
                                      Collectors.joining()
                              )
                ;
    }
}
