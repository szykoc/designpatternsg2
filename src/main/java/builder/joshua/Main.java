package builder.joshua;




public class Main {

    public static void main(String[] args) {
        Computer computer = new Computer
                .Builder(1.4, "Mac-os")
                .build();

        System.out.println(computer);
    }

}
