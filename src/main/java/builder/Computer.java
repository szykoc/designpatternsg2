package builder;

import java.time.LocalDate;

public class Computer {
    private double ghz;
    private int ram;
    private double screenInches;
    private LocalDate productionDate;
    private double weight;
    private String operatingSystem;

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    private String brand;

    public double getGhz() {
        return ghz;
    }

    public void setGhz(double ghz) {
        this.ghz = ghz;
    }

    public int getRam() {
        return ram;
    }

    public void setRam(int ram) {
        this.ram = ram;
    }

    public double getScreenInches() {
        return screenInches;
    }

    public void setScreenInches(double screenInches) {
        this.screenInches = screenInches;
    }

    public LocalDate getProductionDate() {
        return productionDate;
    }

    public void setProductionDate(LocalDate productionDate) {
        this.productionDate = productionDate;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public String getOperatingSystem() {
        return operatingSystem;
    }

    public void setOperatingSystem(String operatingSystem) {
        this.operatingSystem = operatingSystem;
    }

    @Override
    public String toString() {
        return "Computer{" +
                "ghz=" + ghz +
                ", ram=" + ram +
                ", screenInches=" + screenInches +
                ", productionDate=" + productionDate +
                ", weight=" + weight +
                ", operatingSystem='" + operatingSystem + '\'' +
                ", brand='" + brand + '\'' +
                '}';
    }
}
