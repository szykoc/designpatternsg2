package builder.inter;


import builder.Computer;

import java.time.LocalDate;

public class Main {
    public static void main(String[] args) {

        Computer computer = new ComputerBuilderImpl()
                .ram(2048)
                .ghz(2.4)
                .screenInches(27.0)
                .weight(2.2)
                .brand("Lenovo")
                .operatingSystem("Windows")
                .productionDate(LocalDate.of(2019, 5, 2))
                .build();

        System.out.println(computer);
    }
}
