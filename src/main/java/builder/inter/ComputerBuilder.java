package builder.inter;

import builder.Computer;

import java.time.LocalDate;

public interface ComputerBuilder {
    Computer build();
    ComputerBuilder ghz(double ghz);
    ComputerBuilder ram(int ram);
    ComputerBuilder screenInches(double screenInches);
    ComputerBuilder productionDate(LocalDate productionDate);
    ComputerBuilder weight(double weight);
    ComputerBuilder operatingSystem(String operatingSystem);
    ComputerBuilder brand(String brand);
    // zawsze zwracamy this - ComputerBuilder
    // wtedy możemy korzystać fluent builder
    //   .ghz
    //   .ram
    //   .weight   etc...
}
