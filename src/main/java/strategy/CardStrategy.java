package strategy;


public class CardStrategy implements PaymentStrategy{
    @Override
    public void payMethod() {
        System.out.println("Płatność kartą");
    }
}
