package strategy;


public interface PaymentStrategy {
    default void test(){};
    void payMethod();
}
