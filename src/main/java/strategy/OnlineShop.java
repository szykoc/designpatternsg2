package strategy;


public class OnlineShop {

    private PaymentStrategy paymentStrategy;

    public OnlineShop(PaymentStrategy paymentStrategy) {
        this.paymentStrategy = paymentStrategy;
    }

    public void paymentMethod(){
        System.out.println("Wybrałeś metode płatoności: ");
        paymentStrategy.payMethod();
    }

    public OnlineShop setPaymentStrategy(PaymentStrategy paymentStrategy) {
        this.paymentStrategy = paymentStrategy;
        return this;
    }
}
