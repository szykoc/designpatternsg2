package strategy;

public class Main {

    public static void main(String[] args) {
        PaymentStrategy cashStrategy = new CashStrategy();

        OnlineShop shop = new OnlineShop(cashStrategy);

        shop.paymentMethod();

        shop.setPaymentStrategy(new CardStrategy());
        shop.paymentMethod();
    }
}
