package strategy;


public class CashStrategy implements PaymentStrategy {
    @Override
    public void payMethod() {
        System.out.println("Płatność gotówką");
    }
}
